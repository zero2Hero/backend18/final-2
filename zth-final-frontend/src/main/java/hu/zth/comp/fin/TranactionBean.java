package hu.zth.comp.fin;

import hu.zth.comp.fin.model.Account;
import hu.zth.comp.fin.model.Transaction;
import hu.zth.comp.fin.model.TransactionStatus;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class TranactionBean implements Serializable {

    @Inject
    private BackendSessionBean backend;

    @Inject
    private IndexBean indexBean;

    public List<Transaction> getLast10Transactions() {
        Long userId = backend.getUserService().getUser().getId();
        List<Account> accounts = backend.getAccountService().getAccounts(userId);
        List<Transaction> transactions = new ArrayList<>();
        accounts.forEach(account -> transactions.addAll(backend.getTransactionService().getLast10Transactions(account.getId())));
        transactions.sort((o1, o2) -> o2.getId().compareTo(o1.getId()));
        return transactions;
    }

    public void sendTransactionRequest(Long sourceAccountId, Long destinationAccountId, Double amount, String comment) {
        Response response = backend.getTransactionService().requestTransaction(sourceAccountId, destinationAccountId, amount, comment);
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            indexBean.setInfo("Transaction request has been sent (" + sourceAccountId + " -> " + destinationAccountId + " with amount " + amount + "), comment: " + response.readEntity(String.class));
        } else {
            indexBean.setError("Transaction request error: " + response.readEntity(String.class));
        }
        response.close();
    }
    
    public boolean isPending(Transaction transaction) {
        return transaction.getStatus() == TransactionStatus.PENDING;
    }
    
    public void export(Account account) throws IOException {
        Response response = backend.getTransactionService().exportTransactions(account.getId());
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            indexBean.setInfo("Exported account: " + account.getId());
            createDownload(response.readEntity(String.class), account.getId());
        } else {
            indexBean.setError("Export error: " + response.readEntity(String.class));
        }
        
        response.close();
    }
    
    private void createDownload(String exportedXml, Long accountId) throws IOException {
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletResponse resp = (HttpServletResponse) fc.getExternalContext().getResponse();

        resp.setContentType("application/octet-stream");
        resp.addHeader("Content-Disposition", "attachment; filename=\"account-" + accountId + ".xml\"");

        PrintWriter writer = resp.getWriter();
        writer.append(exportedXml);

        writer.close();
        FacesContext.getCurrentInstance().responseComplete();
    }

}
