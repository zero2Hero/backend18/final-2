package hu.zth.comp.fin;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.undertow.WARArchive;

import java.io.File;

public class FrontendApp {
    
    private static final String ROOT = "zth-final-frontend/";
    private static final String WEBAPP = ROOT + "src/main/webapp/";
    private static final String WEBINF = WEBAPP + "WEB-INF/";
    
    public static void main(String[] args) throws Exception {
        new Swarm(false, args).
                withConfig(FrontendApp.class.getClassLoader().getResource("project-defaults.yml")).
                start(ShrinkWrap.create(WARArchive.class, "zth-final-frontend.war").
                        addPackages(true, "hu.zth.comp").
                        setWebXML(new File(WEBINF, "web.xml")).
                        addAsWebResource(new File(WEBAPP, "index.xhtml")).
                        addAsWebResource(new File(WEBAPP, "login.xhtml")).
                        addAsWebResource(new File(WEBAPP, "error.xhtml"))
                );
    }
    
}
