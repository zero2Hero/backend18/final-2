package hu.zth.comp.fin;

import hu.zth.comp.fin.model.UserRole;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.Serializable;

@Named
@SessionScoped
public class LoginBean implements Serializable {
    
    public static final String LOGIN_XHTML = "/login.xhtml";
    public static final String INDEX_XHTML = "/index.xhtml";
    
    @Inject
    private BackendSessionBean backend;
    
    @Inject
    private IndexBean indexBean;
    
    private String error = null;
    private String loggedInUsername = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getLoggedInUsername() {
        return loggedInUsername;
    }

    public void login(String username, String password) throws IOException {
        if (backend.getUserService().login(username, password).getStatus() == Response.Status.OK.getStatusCode()) {
            error = null;
            loggedInUsername = username;
            FacesContext.getCurrentInstance().getExternalContext().redirect(INDEX_XHTML);
        } else {
            error = "Wrong username or password";
            loggedInUsername = null;
        }
    }
    
    public void logout() throws IOException {
        if (backend.getUserService().logout().getStatus() == Response.Status.OK.getStatusCode()) {
            error = null;
            loggedInUsername = null;
            FacesContext.getCurrentInstance().getExternalContext().redirect(LOGIN_XHTML);
        } else {
            indexBean.setError("Could not log out");
        }
    }
    
    public String getRealName() throws IOException {
        try {
            return backend.getUserService().getUser().getRealname();
        } catch (NotAuthorizedException e) {
            error = "Session lost, login again";
            FacesContext.getCurrentInstance().getExternalContext().redirect(LOGIN_XHTML);
            return "";
        }
    }

    public boolean isAdmin() {
        return backend.getUserService().getUser().getRole() == UserRole.ADMIN;
    }
    
}
