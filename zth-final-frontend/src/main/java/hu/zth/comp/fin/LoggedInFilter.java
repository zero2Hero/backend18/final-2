package hu.zth.comp.fin;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(urlPatterns = "/*")
public class LoggedInFilter implements Filter {
    
    @Inject
    private LoginBean loginBean;
    
    @Override
    public void init(FilterConfig filterConfig) {
        // unused
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (isLoginRequired(request)) {
            loginBean.setError(null);
            ((HttpServletResponse) response).sendRedirect(LoginBean.LOGIN_XHTML);
        } else {
            chain.doFilter(request, response);
        }
    }
    
    private boolean isLoginRequired(ServletRequest request) {
        return loginBean.getLoggedInUsername() == null && !LoginBean.LOGIN_XHTML.equals(((HttpServletRequest) request).getRequestURI());
    }

    @Override
    public void destroy() {
        // unused
    }
}
