package hu.zth.comp.fin;

import org.jboss.logging.Logger;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class IndexBean implements Serializable {
    
    private static final Logger log = Logger.getLogger(IndexBean.class);
    
    private String error;
    private String info;

    public String getErrorAndClear() {
        String errorMessage = this.error;
        this.error = null;
        return errorMessage;
    }

    public String getError() {
        return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }

    public String getInfoAndClear() {
        String infoMessage = this.info;
        this.info = null;
        return infoMessage;
    }
    
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
    
    public void refresh() {
        log.debug("Refresh clicked");
    }
}
