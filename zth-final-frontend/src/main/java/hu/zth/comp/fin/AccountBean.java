package hu.zth.comp.fin;

import hu.zth.comp.fin.model.Account;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.core.Response;
import java.io.Serializable;
import java.util.List;

@Named
@RequestScoped
public class AccountBean implements Serializable {

    @Inject
    private BackendSessionBean backend;
    
    @Inject
    private IndexBean indexBean;

    public void topUp(Long accountId, Double amount) {
        Response topup = backend.getAccountService().topup(accountId, amount);
        if (topup.getStatus() == Response.Status.FORBIDDEN.getStatusCode()) {
            indexBean.setError("Operation forbidden, admin role is required");
        } else {
            indexBean.setInfo("Account topUp with ID " + accountId + " success with amount " + amount);
        }
        topup.close();
    }
    
    public List<Account> getAccounts() {
        return backend.getAccountService().getAccounts(backend.getUserService().getUser().getId());
    }
    
}
