package hu.zth.comp.fin;

import hu.zth.comp.fin.api.AccountService;
import hu.zth.comp.fin.api.TransactionService;
import hu.zth.comp.fin.api.UserService;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.ws.rs.core.UriBuilder;
import java.io.Serializable;

@SessionScoped
public class BackendSessionBean implements Serializable {

    private static final String BACKEND_BASE_URL = System.getProperty("zth.backend.url", "http://localhost:8000");
    
    private transient UserService userService;
    private transient AccountService accountService;
    private transient TransactionService transactionService;

    @PostConstruct
    private void init() {
        reinitRestClient();
    }
    
    public void reinitRestClient() {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(UriBuilder.fromPath(BACKEND_BASE_URL));
        userService = target.proxy(UserService.class);
        accountService = target.proxy(AccountService.class);
        transactionService = target.proxy(TransactionService.class);
    }

    public UserService getUserService() {
        return userService;
    }

    public AccountService getAccountService() {
        return accountService;
    }

    public TransactionService getTransactionService() {
        return transactionService;
    }
}
