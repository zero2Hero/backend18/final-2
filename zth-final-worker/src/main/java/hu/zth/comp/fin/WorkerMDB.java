package hu.zth.comp.fin;

import hu.zth.comp.fin.api.TransactionRequest;
import org.jboss.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@MessageDriven(name = "WorkerMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/jms/queue/process-transaction"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
public class WorkerMDB implements MessageListener {
    
    private static final Logger log = Logger.getLogger(WorkerMDB.class);
    
    @EJB
    private WorkerService service;

    @Override
    public void onMessage(Message message) {
        try {
            if (message.isBodyAssignableTo(TransactionRequest.class)) {
                TransactionRequest transactionRequest = message.getBody(TransactionRequest.class);
                service.processTransactionRequest(transactionRequest);
            } else {
                log.error("Unsupported message type");
            }
        } catch (JMSException e) {
            log.errorv(e, "Could not process received message");
        }
    }
    
}
