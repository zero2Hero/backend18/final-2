package hu.zth.comp.fin;

import hu.zth.comp.fin.api.TransactionRequest;
import hu.zth.comp.fin.api.TransactionResponse;
import org.jboss.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import java.security.SecureRandom;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;

@Stateless
public class WorkerService {
    
    private static final Logger log = Logger.getLogger(WorkerService.class);
    
    @Inject
    private JMSContext context;
    
    @Resource(lookup = "java:/jms/queue/transaction-processed")
    private Queue transactionProcessed;
    
    private static final SecureRandom RND = new SecureRandom();
    
    public void processTransactionRequest(TransactionRequest request) {
        TransactionResponse response = new TransactionResponse(request);
        log.infov("Received transaction request ({0})", request.getTransactionId());
        check(request, response);
        waitSomeTime();
        sendResponse(response);
    }
    
    private void waitSomeTime() {
        long secondsToWait = RND.nextInt(30) + 30L;
        LockSupport.parkNanos(TimeUnit.SECONDS.toNanos(secondsToWait));
    }
    
    private void sendResponse(TransactionResponse response) {
        ObjectMessage responseMessage = context.createObjectMessage(response);
        context.createProducer().send(transactionProcessed, responseMessage);
        log.infov("Transaction response sent ({0})", response.getRequest().getTransactionId());
    }
    
    private void checkLuck(TransactionResponse response) {
        if (System.currentTimeMillis() % 3 == 0) {
            response.setFailed("No luck with, try again later");
        } else {
            response.setSuccess();
        }
    }
    
    private void check(TransactionRequest request, TransactionResponse response) {
        Double amount = request.getAmount();
        if (request.getSourceAccountBalance() - amount > 1000) {
            checkLuck(response);
        } else {
            response.setFailed("Source account balance must be more than 1000, cannot complete transaction with amount: " + amount); 
        }
    }
    
}
