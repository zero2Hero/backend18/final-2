package hu.zth.comp.fin;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.undertow.WARArchive;

public class WorkerApp {

    public static void main(String[] args) throws Exception {
        new Swarm(false, args).
                withConfig(WorkerApp.class.getClassLoader().getResource("project-defaults.yml")).
                start(ShrinkWrap.create(WARArchive.class, "zth-final-worker.war").
                        addPackages(true, "hu.zth.comp"));
    }
    
}
