package hu.zth.comp.fin.service.dao;

import hu.zth.comp.fin.model.BaseEntity;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class BaseDao {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public <E extends BaseEntity> E findById(Long id, Class<E> entityClass) {
        return entityManager.find(entityClass, id);
    }
    
    public EntityManager getEntityManager() {
        return entityManager;
    }
    
    public <E extends BaseEntity> E save(E entity) {
        if (entity.getId() == null) {
            entityManager.persist(entity);
            return entity;
        } else {
            return entityManager.merge(entity);
        }
    }
    
}
