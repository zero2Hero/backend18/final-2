package hu.zth.comp.fin.service.dao;

import hu.zth.comp.fin.model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class UserDao {
    
    @EJB
    private BaseDao dao;

    public User getUserByUsername(String username) {
        return dao.getEntityManager().
                createNamedQuery(User.BY_USERNAME, User.class).
                setParameter("username", username).
                getSingleResult();
    }
}
