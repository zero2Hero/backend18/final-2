package hu.zth.comp.fin.service.dao;

import hu.zth.comp.fin.api.TransactionRequest;
import hu.zth.comp.fin.api.TransactionResponse;
import hu.zth.comp.fin.model.Account;
import hu.zth.comp.fin.model.Transaction;
import hu.zth.comp.fin.model.TransactionStatus;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import java.util.Date;
import java.util.List;

@Stateless
public class TransactionDao {

    @EJB
    private BaseDao baseDao;

    @EJB
    private AccountDao accountDao;

    public Transaction findById(Long id) {
        return baseDao.findById(id, Transaction.class);
    }

    public List<Transaction> getLast10Transactions(Long accountId) {
        return baseDao.getEntityManager().
                createNamedQuery(Transaction.BY_ACCOUNT, Transaction.class).
                setParameter("account_id", accountId).
                setMaxResults(10).
                getResultList();
    }

    public TransactionRequest startTransaction(Long sourceAccountId, Long destinationAccountId, Double amount, String comment) {
        TransactionRequest request = new TransactionRequest();
        request.setComment(comment);

        Account sourceAccount = accountDao.findById(sourceAccountId);
        Account destinationAccount = accountDao.findById(destinationAccountId);

        request.setSourceAccountBalance(sourceAccount.getBalance());
        request.setDestinationAccountBalance(destinationAccount.getBalance());
        request.setSourceAccountId(sourceAccountId);
        request.setDestinationAccountId(destinationAccountId);
        request.setAmount(amount);

        Transaction transaction = createTransaction(request, sourceAccount, destinationAccount);
        request.setTransactionId(transaction.getId());
        return request;
    }

    public Transaction createTransaction(TransactionRequest request, Account sourceAccount, Account destinationAccount) {
        Transaction transaction = new Transaction();
        transaction.setSourceAccount(sourceAccount);
        transaction.setDestinationAccount(destinationAccount);
        transaction.setAmount(request.getAmount());
        transaction.setComment(request.getComment());
        return baseDao.save(transaction);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void finishTransaction(TransactionResponse response) {
        Transaction transaction = findById(response.getRequest().getTransactionId());
        if (response.isSuccess()) {
            finishSuccess(transaction);
        } else {
            finishFailed(transaction, response.getError());
        }
        transaction.setFinished(new Date());
    }

    private void finishSuccess(Transaction transaction) {
        Double amount = transaction.getAmount();
        Double sourceBalance = transaction.getSourceAccount().getBalance();
        Double destinationBalance = transaction.getDestinationAccount().getBalance();
        transaction.getSourceAccount().setBalance(sourceBalance - amount);
        transaction.getDestinationAccount().setBalance(destinationBalance + amount);
        transaction.setStatus(TransactionStatus.SUCCESS);
    }

    private void finishFailed(Transaction transaction, String error) {
        transaction.setErrorMessage(error);
        transaction.setStatus(TransactionStatus.FAILED);
    }

}
