package hu.zth.comp.fin.service.rest;

import hu.zth.comp.fin.api.TransactionRequest;
import hu.zth.comp.fin.api.TransactionService;
import hu.zth.comp.fin.model.Account;
import hu.zth.comp.fin.model.Transaction;
import hu.zth.comp.fin.model.Transactions;
import hu.zth.comp.fin.service.dao.AccountDao;
import hu.zth.comp.fin.service.dao.TransactionDao;
import org.jboss.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.Principal;
import java.util.List;

@RequestScoped
@Path("/api/trans/")
public class TransactionRestService implements TransactionService {
    
    private static final Logger log = Logger.getLogger(TransactionRestService.class);

    @EJB
    private TransactionDao transactionDao;

    @EJB
    private AccountDao accountDao;

    @Context
    private HttpServletRequest httpServletRequest;

    @Inject
    @JMSConnectionFactory("java:/jms/remote-mq")
    private JMSContext context;

    @Resource(mappedName = "jms/queue/process-transaction")
    private Queue processTransaction;

    @Override
    public List<Transaction> getLast10Transactions(Long accountId) {
        return transactionDao.getLast10Transactions(accountId);
    }

    @Override
    public Response requestTransaction(Long sourceAccountId, Long destinationAccountId, Double amount, String comment) {
        if (!checkUserAccount(sourceAccountId)) {
            return Response.status(Response.Status.FORBIDDEN).
                    entity(String.format("Source account [%d] is not owned by the sender [%s]",
                            sourceAccountId, httpServletRequest.getUserPrincipal().getName())).
                    build();
        }
        try {
            TransactionRequest request = transactionDao.startTransaction(sourceAccountId, destinationAccountId, amount, comment);
            ObjectMessage requestMessage = context.createObjectMessage(request);
            context.createProducer().send(processTransaction, requestMessage);
            return Response.ok(request.getTransactionId()).build();
        } catch (Exception e) {
            log.errorv(e, "Could not create transaction request");
            return Response.serverError().entity(e.getMessage()).build();
        }
    }
    
    @Override
    public Response exportTransactions(Long accountId) {
        try {
            List<Transaction> last10Transactions = transactionDao.getLast10Transactions(accountId);
            Transactions transactions = new Transactions();
            transactions.setTransactions(last10Transactions);
            return Response.ok(transactions, MediaType.APPLICATION_XML_TYPE).build();
        } catch (Exception e) {
            log.errorv(e, "Could not export transactions for account {0}", accountId);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    private boolean checkUserAccount(Long sourceAccountId) {
        Account account = accountDao.findById(sourceAccountId);
        if (account != null) {
            String username = account.getUser().getUsername();
            Principal userPrincipal = httpServletRequest.getUserPrincipal();
            if (userPrincipal != null) {
                return username.equals(userPrincipal.getName());
            }
        }
        return false;
    }
}
