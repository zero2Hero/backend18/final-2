package hu.zth.comp.fin;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/")
public class BackendRestApp extends Application {

}
