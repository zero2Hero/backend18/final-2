package hu.zth.comp.fin.service.rest;

import hu.zth.comp.fin.api.UserService;
import hu.zth.comp.fin.service.dao.UserDao;
import hu.zth.comp.fin.model.User;
import org.jboss.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Path("/")
@RequestScoped
public class UserRestService implements UserService {
    
    private static final Logger log = Logger.getLogger(UserRestService.class);

    @Context
    private HttpServletRequest request;
    
    @EJB
    private UserDao dao;
    
    @Override
    public Response login(String username, String password) {
        try {
            request.login(username, password);
            return Response.ok().build();
        } catch (ServletException e) {
            log.warnv(e, "Failed authentication request with username: {0}", username);
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @Override
    public Response logout() {
        try {
            request.logout();
            return Response.ok().build();
        } catch (ServletException e) {
            log.errorv(e, "Could not log out current user");
            return Response.serverError().build();
        }
    }

    @Override
    public User getUser() {
        String loggedInUsername = request.getUserPrincipal().getName();
        try {
            return dao.getUserByUsername(loggedInUsername);
        } catch (NoResultException e) {
            log.errorv(e, "No user found in the db with session username: {0}", loggedInUsername);
            return new User();
        }
    }
}
