package hu.zth.comp.fin.service.dao;

import hu.zth.comp.fin.model.Account;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class AccountDao {
    
    @EJB
    private BaseDao dao;
    
    public Account findById(Long id) {
        return dao.findById(id, Account.class);
    }

    public List<Account> byUserId(Long id) {
        return dao.getEntityManager().
                createNamedQuery(Account.BY_USER_ID, Account.class).
                setParameter("id", id).
                getResultList();
    }
    
    public void topUp(Long accountId, Double amount) {
        Account account = findById(accountId);
        account.setBalance(account.getBalance() + amount);
    }
    
}
