package hu.zth.comp.fin;

import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.wildfly.swarm.Swarm;
import org.wildfly.swarm.undertow.WARArchive;

public class BackendApp {

    public static void main(String[] args) throws Exception {
        new Swarm(false, args).
                withConfig(BackendApp.class.getClassLoader().getResource("project-defaults.yml")).
                start(ShrinkWrap.create(WARArchive.class, "zth-final-backend.war").
                        addPackages(true, "hu.zth.comp").
                        addAsResource("META-INF/orm.xml").
                        addAsResource("META-INF/persistence.xml"));
    }

}
