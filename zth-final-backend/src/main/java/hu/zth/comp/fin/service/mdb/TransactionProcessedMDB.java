package hu.zth.comp.fin.service.mdb;

import hu.zth.comp.fin.api.TransactionResponse;
import hu.zth.comp.fin.service.dao.TransactionDao;
import org.jboss.ejb3.annotation.ResourceAdapter;
import org.jboss.logging.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;

@ResourceAdapter("remote-mq")
@MessageDriven(name = "TransactionProcessedMDB", activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/jms/queue/transaction-processed"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")})
public class TransactionProcessedMDB implements MessageListener {
    
    private static final Logger log = Logger.getLogger(TransactionProcessedMDB.class);

    @EJB
    private TransactionDao transactionDao;

    /**
     * A kapott TransactionResponse alapján végezzük el a szükséges adatbázis műveleteket!
     * 
     * @param message
     */
    @Override
    public void onMessage(Message message) {
        try {
            if (message.isBodyAssignableTo(TransactionResponse.class)) {
                TransactionResponse transactionResponse = message.getBody(TransactionResponse.class);
                transactionDao.finishTransaction(transactionResponse);
            } else {
                log.error("Unsupported message type");
            }
        } catch (JMSException e) {
            log.errorv(e, "Could not process received message");
        }
    }
    
}
