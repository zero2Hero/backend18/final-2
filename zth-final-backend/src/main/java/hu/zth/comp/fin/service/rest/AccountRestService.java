package hu.zth.comp.fin.service.rest;

import hu.zth.comp.fin.api.AccountService;
import hu.zth.comp.fin.service.dao.AccountDao;
import hu.zth.comp.fin.model.Account;
import org.jboss.logging.Logger;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import java.util.List;

@RequestScoped
@Path("/api/account/")
public class AccountRestService implements AccountService {
    
    private static final Logger log = Logger.getLogger(AccountRestService.class);

    @EJB
    private AccountDao dao;

    @Override
    public Response topup(Long accountId, Double amount) {
        try {
            dao.topUp(accountId, amount);
            return Response.ok().build();
        } catch (Exception e) {
            log.warnv(e, "Could not modify account {0}", accountId);
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @Override
    public List<Account> getAccounts(Long userId) {
        return dao.byUserId(userId);
    }
}
