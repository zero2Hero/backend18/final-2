alter sequence zth.zth_id_gen restart with 1000;
insert into zth.user (id, username, password, role, realname) values (100, 'admin', 's3cret', 'ADMIN', 'The Admin');
insert into zth.user (id, username, password, role, realname) values (101, 'user1', 'pass1', 'USER', 'Bruce Banner');
insert into zth.user (id, username, password, role, realname) values (102, 'user2', 'pass2', 'USER', 'Robert Smith');
insert into zth.user (id, username, password, role, realname) values (103, 'user3', 'pass3', 'USER', 'Peter Parker');
insert into zth.user (id, username, password, role, realname) values (104, 'user4', 'pass4', 'USER', 'Tony Stark');


insert into zth.account (id, account_name, user_id, balance) values (200, 'Default', 100, 1000000);
insert into zth.account (id, account_name, user_id, balance) values (201, 'Hulk''s account', 101, 30200);
insert into zth.account (id, account_name, user_id, balance) values (202, 'TheCure account', 102, 52800);
insert into zth.account (id, account_name, user_id, balance) values (203, 'Spiderman''s account', 103, 375);
insert into zth.account (id, account_name, user_id, balance) values (204, 'Private', 104, 5999999630);
insert into zth.account (id, account_name, user_id, balance) values (205, 'Ironman', 104, 1000);


insert into zth.transaction (id, amount, acc_source, acc_dest, status, requested, finished, comment, error_message)
values (300, 370, 204, 203, 'SUCCESS', '2018-02-22 22:10:06', '2018-02-23 14:01:58', 'Rugdalózóra mosására', null);

insert into zth.transaction (id, amount, acc_source, acc_dest, status, requested, finished, comment, error_message)
values (301, 10000, 205, 201, 'FAILED', '2018-01-22 22:10:06', '2018-01-23 14:01:58', 'Nyugtatóra', 'Hulk dühös volt :(');
