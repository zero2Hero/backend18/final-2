package hu.zth.comp.fin.api;

import java.io.Serializable;

public class TransactionRequest implements Serializable {
    
    private Long sourceAccountId;
    private Double sourceAccountBalance;
    private Long destinationAccountId;
    private Double destinationAccountBalance;
    private Double amount;
    private Long transactionId;
    private String comment;

    public Long getSourceAccountId() {
        return sourceAccountId;
    }

    public void setSourceAccountId(Long sourceAccountId) {
        this.sourceAccountId = sourceAccountId;
    }

    public Double getSourceAccountBalance() {
        return sourceAccountBalance;
    }

    public void setSourceAccountBalance(Double sourceAccountBalance) {
        this.sourceAccountBalance = sourceAccountBalance;
    }

    public Long getDestinationAccountId() {
        return destinationAccountId;
    }

    public void setDestinationAccountId(Long destinationAccountId) {
        this.destinationAccountId = destinationAccountId;
    }

    public Double getDestinationAccountBalance() {
        return destinationAccountBalance;
    }

    public void setDestinationAccountBalance(Double destinationAccountBalance) {
        this.destinationAccountBalance = destinationAccountBalance;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Long transactionId) {
        this.transactionId = transactionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
