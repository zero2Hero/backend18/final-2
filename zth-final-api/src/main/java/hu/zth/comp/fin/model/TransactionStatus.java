package hu.zth.comp.fin.model;

public enum TransactionStatus {
    PENDING("Pending"),
    SUCCESS("Success"),
    FAILED("Failed");
    
    private String label;
    
    TransactionStatus(String label) {
        this.label = label;
    }
    
    public String label() {
        return label;
    }
}
