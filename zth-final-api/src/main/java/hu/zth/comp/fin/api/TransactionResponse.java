package hu.zth.comp.fin.api;

import java.io.Serializable;

public class TransactionResponse implements Serializable {
    
    private TransactionRequest request;
    private boolean success;
    private String error;

    public TransactionResponse(TransactionRequest request) {
        this.request = request;
    }

    public TransactionRequest getRequest() {
        return request;
    }

    public boolean isSuccess() {
        return success;
    }

    public String getError() {
        return error;
    }

    public void setFailed(String error) {
        this.success = false;
        this.error = error;
    }
    
    public void setSuccess() {
        this.success = true;
        this.error = null;
    }
}
