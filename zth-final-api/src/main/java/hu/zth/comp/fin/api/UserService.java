package hu.zth.comp.fin.api;

import hu.zth.comp.fin.model.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/")
public interface UserService {

    /**
     * A megadott felasználó név és jelszó szertint, amennyiben az érvényes,
     * be kell jelenkeztetnie a felasználót az adott session-be, security context-be
     * (jaas, login module)
     * @return
     */
    @POST
    @Path("login")
    Response login(@FormParam("username") String username,
                   @FormParam("password") String password);


    /**
     * Az aktuálisan bejelentkezett felhasználüt kell kijelentkeztetnie
     * @return
     */
    @GET
    @Path("logout")
    Response logout();

    /**
     * Az aktuálisan bejelentkezett felhasználóhoz tartozó User entitást adja vissza.
     * Csak akkor kerül meghívásra, ha van bejelentkezett felhasználó
     * 
     * @return
     */
    @GET
    @Path("api/user")
    @Produces(MediaType.APPLICATION_JSON)
    User getUser();
    
}
