package hu.zth.comp.fin.model;

import javax.xml.bind.annotation.*;
import java.util.List;

@SuppressWarnings("squid:S1700")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Transactions {
    
    @XmlElements(@XmlElement(name = "transaction"))
    private List<Transaction> transactions;

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
