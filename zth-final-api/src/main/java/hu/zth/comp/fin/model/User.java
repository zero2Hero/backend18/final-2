package hu.zth.comp.fin.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Objects;
import java.util.Set;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries(@NamedQuery(name = User.BY_USERNAME, query = "select u from User u where u.username = :username"))
public class User extends BaseEntity {
    
    public static final String BY_USERNAME = "user.by_username";
    
    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false)
    private String realname;
    
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<Account> accounts;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return Objects.equals(getUsername(), user.getUsername()) &&
                getRole() == user.getRole() &&
                Objects.equals(getPassword(), user.getPassword()) &&
                Objects.equals(getRealname(), user.getRealname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getUsername(), getRole(), getPassword(), getRealname());
    }
}
