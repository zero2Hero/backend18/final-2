package hu.zth.comp.fin.model;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserAdapter extends XmlAdapter<String, User> {

    private static final Pattern USER_PATTERN = Pattern.compile("\\(([0-9]+) - (.+)\\) (.+)");
    private static final String USER_FORMAT = "(%d - %s) %s";

    @Override
    public User unmarshal(String v) throws Exception {
        Matcher matcher = USER_PATTERN.matcher(v);
        if (matcher.find()) {
            User user = new User();
            user.setId(Long.parseLong(matcher.group(1)));
            user.setUsername(matcher.group(2));
            user.setRealname(matcher.group(3));
            return user;
        }
        throw new IllegalStateException("Cannot parse user format: " + v);
    }

    @Override
    public String marshal(User v) throws Exception {
        return String.format(USER_FORMAT, v.getId(), v.getUsername(), v.getRealname());
    }
}
