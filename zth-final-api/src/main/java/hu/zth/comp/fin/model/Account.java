package hu.zth.comp.fin.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Objects;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries(@NamedQuery(name = Account.BY_USER_ID, query = "select a from Account a where a.user.id = :id"))
public class Account extends BaseEntity {
    
    public static final String BY_USER_ID = "account.by_user_id";

    @XmlJavaTypeAdapter(UserAdapter.class)
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "account_name", nullable = false)
    private String name;

    @Column(nullable = false)
    private Double balance;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Account account = (Account) o;
        return Objects.equals(getUser().getId(), account.getUser().getId()) &&
                Objects.equals(getName(), account.getName()) &&
                Objects.equals(getBalance(), account.getBalance());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getUser().getId(), getName(), getBalance());
    }
}
