package hu.zth.comp.fin.model;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.util.Date;
import java.util.Objects;

@Entity
@XmlAccessorType(XmlAccessType.FIELD)
@NamedQueries(@NamedQuery(name = Transaction.BY_ACCOUNT, query = "select t from Transaction t where t.sourceAccount.id = :account_id or t.destinationAccount.id = :account_id order by t.requested desc"))
public class Transaction extends BaseEntity {
    
    public static final String BY_ACCOUNT = "transaction.by_account";
    
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TransactionStatus status = TransactionStatus.PENDING;
    
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date requested = new Date();

    @Column(name = "finished")
    @Temporal(TemporalType.TIMESTAMP)
    private Date finished;
    
    @ManyToOne
    @JoinColumn(name = "acc_source", nullable = false)
    private Account sourceAccount;

    @ManyToOne
    @JoinColumn(name = "acc_dest", nullable = false)
    private Account destinationAccount;

    @Column(nullable = false)
    private Double amount;
    
    @Column
    private String comment;
    
    @Column(name = "error_message")
    private String errorMessage;

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }

    public Date getRequested() {
        return requested;
    }

    public void setRequested(Date requested) {
        this.requested = requested;
    }

    public Date getFinished() {
        return finished;
    }

    public void setFinished(Date finished) {
        this.finished = finished;
    }

    public Account getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(Account sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public Account getDestinationAccount() {
        return destinationAccount;
    }

    public void setDestinationAccount(Account destinationAccount) {
        this.destinationAccount = destinationAccount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Transaction that = (Transaction) o;
        return getStatus() == that.getStatus() &&
                Objects.equals(getRequested(), that.getRequested()) &&
                Objects.equals(getFinished(), that.getFinished()) &&
                Objects.equals(getSourceAccount().getId(), that.getSourceAccount().getId()) &&
                Objects.equals(getDestinationAccount().getId(), that.getDestinationAccount().getId()) &&
                Objects.equals(getAmount(), that.getAmount()) &&
                Objects.equals(getComment(), that.getComment()) &&
                Objects.equals(getErrorMessage(), that.getErrorMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getStatus(), getRequested(), getFinished(), getSourceAccount().getId(), getDestinationAccount().getId(), getAmount(), getComment(), getErrorMessage());
    }
}
