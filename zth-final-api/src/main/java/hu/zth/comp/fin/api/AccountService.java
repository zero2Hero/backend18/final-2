package hu.zth.comp.fin.api;

import hu.zth.comp.fin.model.Account;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/account/")
public interface AccountService {

    /**
     * Az adott user id szerint vissza kell adnia a hozza tartozó account-okat
     * 
     * @param userId
     * @return
     */
    @GET
    @Path("list/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    List<Account> getAccounts(@PathParam("userId") Long userId);

    /**
     * A megadott amount értékével növelnie kell az megadott account id-val
     * rendelkező account balance mezőjét amennyiben az aktuális felhasználó
     * admin role-ban van. Ha nincs, akkor hibaüzenetben ezt jelezzük!
     * 
     * @param accountId
     * @param amount
     * @return
     */
    @GET
    @Path("topup/{accountId}/{amount}")
    Response topup(@PathParam("accountId") Long accountId,
                   @PathParam("amount") Double amount);
    
}
