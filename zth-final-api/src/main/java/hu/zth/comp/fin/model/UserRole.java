package hu.zth.comp.fin.model;

public enum UserRole {
    
    USER("User"),
    ADMIN("Admin");
    
    private String label;
    
    UserRole(String label) {
        this.label = label;
    }
    
    public String label() {
        return label;
    }
    
}
