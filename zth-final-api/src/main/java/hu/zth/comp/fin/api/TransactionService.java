package hu.zth.comp.fin.api;

import hu.zth.comp.fin.model.Transaction;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/api/trans/")
public interface TransactionService {

    /**
     * Vissza kell adnia az utolsó 10 tranzakciót, amely a megadott
     * Account ID-hoz tartozik (akár source, akár destination account)
     * Rendezzük úgy, hogy a legújabb tranzakció legyen a lista elején!
     * 
     * @param accountId
     * @return
     */
    @GET
    @Path("last10/{account_id}")
    @Produces(MediaType.APPLICATION_JSON)
    List<Transaction> getLast10Transactions(@PathParam("account_id") Long accountId);

    /**
     * Ellenőrízni kell, hogy az adott source account az aktuálisan bejelentkezett felhasználóhoz tartozik-e!
     * <br/>
     * Ha nem, akkor kuldjünk hibaüzenetet!
     * <br/>
     * Ha igen, akkor a megadott argumentumok alapján létre kell hoznia
     * egy Tranzakciót PENDING állapotban, majd egy ehhez tartozó TransactionRequest -et,
     * amit JMS üzenetként el kell küldeni a "java:/jms/remote-mq" connection factory-n
     * keresztül a "jms/queue/process-transaction" queue-ba!
     * 
     * @param sourceAccountId
     * @param destinationAccountId
     * @param amount
     * @param comment
     * @return
     */
    @POST
    @Path("req")
    Response requestTransaction(@FormParam("source_acc_id") Long sourceAccountId,
                                @FormParam("dest_acc_id") Long destinationAccountId,
                                @FormParam("amount") Double amount,
                                @FormParam("comment") String comment);

    /**
     * Az adott accound ID szerint kérdezzük le a hozzá tartozó tranzakciókat (source és destination is),
     * majd a kapott listából generáljunk XML-t (lásd a frontend implementációját).<br/><br/>
     * 
     * A formázásért lásd a feladat lap negyedik oldalát.<br/><br/>
     * 
     * Roviden:<br/><br/>
     * 
     * transactions tag-en belül transaction lista, melyben az account-okon belül
     * a user a egy tag-ben szerepeljen, a kovetkező formában:<br/>
     * (USER_ID - USER_NAME) FULL_NAME
     * 
     * <br/><br/>
     * 
     * <code>
     * <br/>
     * &lt;transactions&gt;<br/>
     *     &lt;transaction&gt;<br/>
     *         ...<br/>
     *         &lt;sourceAccount&gt;<br/>
     *             &lt;user&gt;(104 - user4) Tony Stark&lt;/user&gt;<br/>
     *         &lt;/sourceAccount&gt;<br/>
     *         ...<br/>
     *     &lt;/transaction&gt;<br/>
     *     &lt;transaction&gt;<br/>
     *         ...<br/>
     *     &lt;/transaction&gt;<br/>
     * &lt;/transactions&gt;<br/>
     * </code>
     * 
     * @param accountId
     * @return
     */
    @GET
    @Path("export/{account_id}")
    @Produces(MediaType.APPLICATION_XML)
    Response exportTransactions(@PathParam("account_id") Long accountId);
    
}
