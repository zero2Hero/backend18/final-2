package hu.zth.comp.fin.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {
    
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "zth_id_gen", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "zth_id_gen", sequenceName = "zth_id_gen")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BaseEntity that = (BaseEntity) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
    
}
